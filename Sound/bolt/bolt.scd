
s.boot;

s.quit;

//synth definition
(
SynthDef.new(\bolt, {
	var sig, env, freq, width;
	env = XLine.kr(1, 0.01, 0.5, doneAction:2);
	freq = XLine.kr(1300, 20, 0.5, doneAction:2);
	width =XLine.kr(0.2, 10, 0.5, doneAction:2);
	sig = Pulse.ar(freq, width) * env;
	Out.ar(0, sig);
}).add
)


x = Synth.new(\bolt);
x.free

//record def

(
    Routine.run {
		var synth, release;
		release = 0.5;
		s.record(path: "~/bolt.wav".standardizePath);
		synth = Synth.new(\bolt);
		release.wait;
		s.stopRecording;
	}
)

//post-processing: export to 32bit wave w/audicity