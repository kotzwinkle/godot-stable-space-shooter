extends Spatial

export var speed = 2.3
export var tilt = 1.8
export var fire_rate = 0.75
export var points = 10
onready var toggle_mode_timer = get_node("ToggleModeTimer")
onready var cooldown_timer = get_node("CooldownTimer")
const SPEED_MULTIPLIER = 10
const MOVEMENT_MODE_FORWARD = 0
const MOVEMENT_MODE_STRAFE = 1
const ACCEL= 2
const DEACCEL= 3
const BOLT_FIRED_SIGNAL_ALIAS = "bolt_fired"
const PLAYER_DESTROYED_SIGNAL_ALIAS = "player_destroyed"
signal player_destroyed(player_global_position)
signal bolt_fired
var velocity = Vector3()
var player
var movement_mode
var can_fire = true

func _ready():
	movement_mode = MOVEMENT_MODE_FORWARD
	set_rotation_deg(Vector3(0, 180, 0))
	set_process(true)
	set_fixed_process(true) 
	initialize_toggle_mode_timer()

func _process(delta):
	if (can_fire):
		can_fire = false
		emit_signal(BOLT_FIRED_SIGNAL_ALIAS,get_muzzle_position())
		initialize_cooldown_timer()

func _fixed_process(delta):
	var direction = self.get_direction() 
	var target = direction * speed * SPEED_MULTIPLIER
	var accel = DEACCEL
	if (direction.dot(velocity) > 0):
		accel = ACCEL
	velocity = velocity.linear_interpolate(target, accel * delta)
	var position = self.get_translation() 
	var red = get_rotation_deg()
	set_rotation_deg(Vector3(0, 180, velocity.x * tilt))
	var translation_velocity = velocity
	set_translation(position + (translation_velocity * delta))
	#move(translation_velocity * delta)

func get_muzzle_position():
	var muzzle = get_node("Muzzle")
	return muzzle.get_global_transform().origin

func get_direction():
	var direction = Vector3(0.0, 0.0, 0.0)
	if(self.movement_mode == MOVEMENT_MODE_STRAFE && player != null):
		var position = self.get_global_transform().origin
		var player_position = player.get_global_transform().origin
		if(player_position.x > position.x):
			direction.x = 1.0
		else:
			direction.x = -1.0
	direction.z = 1.0
	return direction.normalized()

func set_player(player):
	self.player = player

func _on_Area_area_enter( area ):
	var node = area.get_parent()
	if node.is_in_group("player"):
		emit_signal(PLAYER_DESTROYED_SIGNAL_ALIAS, node.get_global_transform().origin)
		remove_node(node)
		remove_node(self)

func remove_node(node):
		node.hide()
		node.queue_free()

func get_wait_time():
	randomize()
	return rand_range(0.5, 1.0)

func initialize_toggle_mode_timer():
		toggle_mode_timer.stop()
		var wait_time = get_wait_time()
		toggle_mode_timer.set_wait_time(wait_time)
		toggle_mode_timer.start()

func initialize_cooldown_timer():
		cooldown_timer.stop()
		if(cooldown_timer.is_active()):
			cooldown_timer.set_active(false)
		var wait_time = fire_rate
		cooldown_timer.set_wait_time(wait_time)
		cooldown_timer.set_active(true)
		cooldown_timer.start()
		
func _on_ToggleModeTimer_timeout():
	if(movement_mode == MOVEMENT_MODE_FORWARD):
		movement_mode = MOVEMENT_MODE_STRAFE
	else:
		movement_mode = MOVEMENT_MODE_FORWARD
	initialize_toggle_mode_timer()

func _on_CooldownTimer_timeout():
	can_fire = true
	cooldown_timer.stop()
	cooldown_timer.set_active(false)
