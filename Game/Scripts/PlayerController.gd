extends Spatial

export var speed = 4.0
export var tilt = 1.5
export var fire_rate = 0.25
export var debug_mode = false
onready var cooldown_timer = get_node("CooldownTimer")
const LEFT_BOUNDARY = -25.0
const RIGHT_BOUNDARY = 25.0
const TOP_BOUNDARY = -65.0
const BOTTOM_BOUNDARY = 17.0
const ACCEL= 2
const DEACCEL= 3
const SPEED_MULTIPLIER = 10.0
const BOLT_FIRED_SIGNAL_ALIAS = "bolt_fired"
signal bolt_fired
var velocity = Vector3()
var can_fire = true

func _ready():
	show_debug()
	set_process(true)
	set_fixed_process(true) 

func _process(delta):
	if (can_fire && Input.is_action_pressed("player_fire")):
		can_fire = false
		emit_signal(BOLT_FIRED_SIGNAL_ALIAS,get_left_muzzle_position())
		emit_signal(BOLT_FIRED_SIGNAL_ALIAS,get_right_muzzle_position())
		initialize_cooldown_timer()

func _fixed_process(delta):
	var direction = self.get_direction()
	var target = direction * speed * SPEED_MULTIPLIER
	var accel
	if (direction.dot(velocity) > 0):
		accel = ACCEL
	else:
		accel = DEACCEL
	velocity = velocity.linear_interpolate(target, accel * delta)
	var position = self.get_translation() 
	var rotation = get_rotation_deg()
	set_rotation_deg(Vector3(rotation.x, rotation.y, velocity.x * -tilt))
	var translation_velocity = velocity
	if((position.x <= LEFT_BOUNDARY && velocity.x < 0)
		|| position.x >= RIGHT_BOUNDARY && velocity.x > 0):
		translation_velocity.x = 0
	if((position.z <= TOP_BOUNDARY && velocity.z < 0)
		|| position.z >= BOTTOM_BOUNDARY && velocity.z > 0):
		translation_velocity.z = 0
	set_translation(position + (translation_velocity * delta))
	#move(translation_velocity * delta)

func initialize_cooldown_timer():
		cooldown_timer.stop()
		if(cooldown_timer.is_active()):
			cooldown_timer.set_active(false)
		var wait_time = fire_rate
		cooldown_timer.set_wait_time(wait_time)
		cooldown_timer.set_active(true)
		cooldown_timer.start()
		
func add_scene_instance(scene, parent, translation):
	scene.set_translation(translation)
	parent.add_child(scene)
	
func get_left_muzzle_position():
	var left_muzzle = get_node("LeftMuzzle")
	return left_muzzle.get_global_transform().origin
	
func get_right_muzzle_position():
	var right_muzzle = get_node("RightMuzzle")
	return right_muzzle.get_global_transform().origin

func get_direction():
	var x = 0.0
	var z = 0.0
	if Input.is_action_pressed("player_move_left"):
		x = -1.0
	elif Input.is_action_pressed("player_move_right"):
		x = 1.0
	if Input.is_action_pressed("player_move_up"):
		z = -1.0
	elif Input.is_action_pressed("player_move_down"):
		z = 1.0
	return Vector3(x, 0, z).normalized()

func print_debug(message):
	if debug_mode:
		print(message)

func show_debug():
	if(speed == 0):
		print_debug("Lawg: speed not set")
	if(tilt == 0):
		print_debug("Lawg: tilt not set")
	if(fire_rate == 0):
		print_debug("Lawg: fire_rate not set")

func _on_CooldownTimer_timeout():
	can_fire = true
	cooldown_timer.stop()
	cooldown_timer.set_active(false)
