extends Popup

export var x_start = 100
onready var resume_button =  get_node("ResumeButton")
onready var restart_button =  get_node("RestartButton")
onready var quit_button =  get_node("QuitButton")
onready var header = get_node("Header")
const PAUSE_MODE = 1
const GAME_OVER_MODE = 2
const WINNER_MODE = 3
const PAUSE_TEXT = "Paused"
const GAME_OVER_TEXT = "Game Over"
const WINNER_TEXT = "You Won!"
var active = false
var menu_items = []
var mode

func _ready():
	self.menu_items = [resume_button, restart_button, quit_button]
	self.set_menu_mode(PAUSE_MODE)
	self.hide()

func open():
	self.active = true
	self.popup()
func close():
	self.active = false
	self.hide()

func set_menu_mode(mode):
	if(mode == PAUSE_MODE || mode == GAME_OVER_MODE || mode == WINNER_MODE):
		self.mode = mode
	if(mode == PAUSE_MODE):
		header.set_text(PAUSE_TEXT)
		set_menu_item_visibility(true, true, true)
	if(mode == GAME_OVER_MODE):
		header.set_text(GAME_OVER_TEXT)
		set_menu_item_visibility(false, true, true)
	if(mode == WINNER_MODE):
		header.set_text(WINNER_TEXT)
		set_menu_item_visibility(false, true, true)
	self.reposition_menu_items()

func set_menu_item_visibility(show_resume_button, show_restart_button, show_quit_button):
	if(show_resume_button):
		resume_button.show()
		resume_button.enabled = true
	else:
		resume_button.hide()
		resume_button.enabled = false
	if(show_restart_button):
		restart_button.show()
		restart_button.enabled = true
	else:
		restart_button.hide()
		restart_button.enabled = false
	if(show_quit_button):
		quit_button.show()
		quit_button.enabled = true
	else:
		quit_button.hide()
		quit_button.enabled = false

func reposition_menu_items():
	if(self.menu_items.size() == 0):
		return
	var visible_item_count = 0
	for i in range(0, self.menu_items.size()):
		if (self.menu_items[i].enabled):
			var position = self.menu_items[i].get_pos()
			position.y = 100 + (visible_item_count * 50)
			self.menu_items[i].set_pos(position)
			visible_item_count +=  1
