extends Node

onready var menu = get_node("Menu")

func _ready():
	set_process_input(true)

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		if(menu.active && menu.mode == menu.PAUSE_MODE):
			resume()
		elif(!menu.active):
			pause()
	
func pause():
	menu.set_exclusive(true)
	menu.set_menu_mode(menu.PAUSE_MODE)
	menu.open()
	get_tree().set_pause(true)

func resume():
	menu.close()
	get_tree().set_pause(false)

func _on_ResumeButton_pressed():
	resume()

func _on_RestartButton_pressed():
	get_tree().set_pause(false)
	get_tree().reload_current_scene()
	menu.close()

func _on_QuitButton_pressed():
	get_tree().quit()

func _on_World_player_died():
	menu.set_exclusive(true)
	menu.set_menu_mode(menu.GAME_OVER_MODE)
	menu.open()
	get_tree().set_pause(true)

func _on_World_player_won():
	menu.set_exclusive(true)
	menu.set_menu_mode(menu.WINNER_MODE)
	menu.open()
	get_tree().set_pause(true)