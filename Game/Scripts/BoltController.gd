extends Spatial

export var speed = 5 
export var debug_mode = false
const SPEED_MULTIPLIER = 10.0 
var z_direction = -1

const METEOR_DESTROYED_SIGNAL_ALIAS = "meteor_destroyed"
const ENEMY_DESTROYED_SIGNAL_ALIAS = "enemy_destroyed"
const PLAYER_DESTROYED_SIGNAL_ALIAS = "player_destroyed"

signal meteor_destroyed(meteor_instance_global_position, points)
signal enemy_destroyed(enemy_instance_global_position)
signal player_destroyed(player_instance_global_position, points)

func _ready():
	set_fixed_process(true)
	
func _fixed_process(delta):
	var position = get_translation() 
	set_translation(position + Vector3(0,0,z_direction * delta * speed * SPEED_MULTIPLIER)); 

func flip_z_direction():
	z_direction *= -1

func _on_Area_area_enter( area ):
	var node = area.get_parent()
	if node.is_in_group("enemies"):
		emit_signal(ENEMY_DESTROYED_SIGNAL_ALIAS, node.get_global_transform().origin, node.points)
		remove_node(node)
		remove_node(self)
	if node.is_in_group("player"):
		emit_signal(PLAYER_DESTROYED_SIGNAL_ALIAS, node.get_global_transform().origin)
		remove_node(node)
		remove_node(self)
	if node.is_in_group("meteors"):
		emit_signal(METEOR_DESTROYED_SIGNAL_ALIAS, node.get_global_transform().origin, node.points)
		remove_node(node)
		remove_node(self)

func remove_node(node):
		node.hide()
		node.queue_free()

func print_debug(message):
	if debug_mode:
		print(message)



