extends Spatial

export var x_spawn_range = 0.0
export var spawn_count = 0
export var debug_mode = false
export var winning_score = 200
onready var spawn_point_node = get_node("/root/Game/World/SpawnPoint")
onready var start_timer = get_node("/root/Game/World/StartTimer")
onready var spawn_rate_timer = get_node("/root/Game/World/SpawnRateTimer")
onready var wave_interval_timer = get_node("/root/Game/World/WaveIntervalTimer")
onready var score_text = get_node("/root/Game/Score/Text")
onready var sound_player = get_node("/root/Game/World/SamplePlayer")
onready var explosion_container = get_node("/root/Game/World/Explosions")
onready var player = get_node("/root/Game/World/Player")
onready var bolts_container = get_node("/root/Game/World/Bolts")
onready var meteors_container = get_node("/root/Game/World/Meteors")
onready var enemy_ship_container = get_node("/root/Game/World/Enemies")
var bolt_scene = load("res://Scenes/Bolt/Bolt.tscn")
var explosion_scene = load("res://Scenes/Explosion/Explosion.tscn")
var meteor_scene = load("res://Scenes/Meteor/Meteor.tscn")
var enemy_ship_scene = load("res://Scenes/EnemyShip/EnemyShip.tscn")
const PLAYER_DIED_SIGNAL_ALIAS = "player_died"
const PLAYER_WON_SIGNAL_ALIAS = "player_won"
signal player_died()
signal player_won()
var base_spawn_position
var hazards_left_in_wave 
var score = 0

func _ready():
	show_debug()
	player.connect(player.BOLT_FIRED_SIGNAL_ALIAS, self, "_on_player_bolt_fired")
	hazards_left_in_wave = spawn_count
	base_spawn_position = spawn_point_node.get_global_transform().origin

func remove_node(node):
		node.hide()
		node.queue_free()

func add_scene_instance(scene, parent, translation):
	scene.set_translation(translation)
	parent.add_child(scene)
	return scene

func add_particle_instance(scene, parent, translation):
	scene.set_translation(translation)
	parent.add_child(scene)
	scene.set_emitting(true)

func spawn_bolt(position, flip_z_direction = false):
	var bolt = bolt_scene.instance()
	if(flip_z_direction):
		bolt.flip_z_direction()
	add_scene_instance(bolt, bolts_container, position)
	bolt.connect(bolt.METEOR_DESTROYED_SIGNAL_ALIAS, self, "_on_meteor_destroyed")
	bolt.connect(bolt.PLAYER_DESTROYED_SIGNAL_ALIAS, self, "_on_player_destroyed")
	bolt.connect(bolt.ENEMY_DESTROYED_SIGNAL_ALIAS, self, "_on_enemy_destroyed")
	sound_player.play("bolt")

func spawn_meteor():
	var position = Vector3(rand_range(-x_spawn_range, x_spawn_range),base_spawn_position.y, base_spawn_position.z)
	var meteor = meteor_scene.instance()
	add_scene_instance(meteor, meteors_container, position)
	meteor.connect(meteor.METEOR_DESTROYED_SIGNAL_ALIAS, self, "_on_meteor_destroyed")
	meteor.connect(meteor.PLAYER_DESTROYED_SIGNAL_ALIAS, self, "_on_player_destroyed")
	meteor.connect(meteor.ENEMY_DESTROYED_SIGNAL_ALIAS, self, "_on_enemy_destroyed")

func spawn_enemy_ship():
	var position = Vector3(rand_range(-x_spawn_range, x_spawn_range),base_spawn_position.y, base_spawn_position.z)
	var enemy_ship = enemy_ship_scene.instance()
	add_scene_instance(enemy_ship, enemy_ship_container, position)
	enemy_ship.connect(enemy_ship.BOLT_FIRED_SIGNAL_ALIAS, self, "_on_enemy_bolt_fired")
	enemy_ship.connect(enemy_ship.PLAYER_DESTROYED_SIGNAL_ALIAS, self, "_on_player_destroyed")
	enemy_ship.set_player(player)
	
func show_debug():
	if(x_spawn_range == 0):
		print_debug("x_spawn_range not set")
	if(spawn_count == 0):
		print_debug("spawn_count not set")

func _on_StartTimer_timeout():
	spawn_rate_timer.start()
	start_timer.stop()
	start_timer.set_active(false)
	print_debug("Game Start")

func _on_SpawnRateTimer_timeout():
	if(hazards_left_in_wave > 0):
		print_debug("Enemy Generated")
		hazards_left_in_wave -= 1
		var position = Vector3(rand_range(-x_spawn_range, x_spawn_range),base_spawn_position.y, base_spawn_position.z)
		if(((randi() % 5) + 1) == 1):
			spawn_enemy_ship()
		else:
			spawn_meteor()
		spawn_rate_timer.stop()
		spawn_rate_timer.start()
	else:
		print_debug("Wave Interval Start")
		spawn_rate_timer.stop()
		spawn_rate_timer.set_active(false)
		if(wave_interval_timer.is_active() == false):
			wave_interval_timer.set_active(true)
		wave_interval_timer.start()

func _on_WaveIntervalTimer_timeout():
	print_debug("Wave Interval Stop")
	wave_interval_timer.stop()
	wave_interval_timer.set_active(false)
	if(spawn_rate_timer.is_active() == false):
		spawn_rate_timer.set_active(true)
	hazards_left_in_wave = spawn_count
	spawn_rate_timer.start()
	
func _on_player_bolt_fired(spawn_position):
	spawn_bolt(spawn_position)

func _on_enemy_bolt_fired(spawn_position):
	spawn_bolt(spawn_position, true)

func add_points(points):
		score += points
		score_text.set_text( str(score) )
		if(score == winning_score):
			emit_signal(PLAYER_WON_SIGNAL_ALIAS)

func _on_meteor_destroyed(meteor_instance_global_position, points = 0):
	add_particle_instance(explosion_scene.instance() , explosion_container, meteor_instance_global_position)
	sound_player.play("explosion")
	if(points > 0):
		add_points(points)
	
func _on_player_destroyed(player_instance_global_position):
	add_particle_instance(explosion_scene.instance() , explosion_container, player_instance_global_position)
	sound_player.play("explosion")
	emit_signal(PLAYER_DIED_SIGNAL_ALIAS)

func _on_enemy_destroyed(enemy_instance_global_position, points = 0):
	add_particle_instance(explosion_scene.instance() , explosion_container, enemy_instance_global_position)
	sound_player.play("explosion")
	if(points > 0):
		add_points(points)

func print_debug(message):
	if debug_mode:
		print(message)

func _on_Boundary_area_exit( area ):
	var node = area.get_parent()
	if node.is_in_group("bolts"):
		print_debug("bolt left play area")
		remove_node(node)
	if node.is_in_group("enemies") :
		print_debug("enemy left play area")
		remove_node(node)
	if node.is_in_group("meteors") :
		print_debug("meteor left play area")
		remove_node(node)
