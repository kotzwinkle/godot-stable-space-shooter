extends RigidBody

export var speed = 2.3
export var debug_mode = false
export var health_points = 1
export var points = 5
const SPEED_MULTIPLIER = 10.0
const METEOR_DESTROYED_SIGNAL_ALIAS = "meteor_destroyed"
const PLAYER_DESTROYED_SIGNAL_ALIAS = "player_destroyed"
const ENEMY_DESTROYED_SIGNAL_ALIAS = "enemy_destroyed"
signal meteor_destroyed(meteor_instance_global_position)
signal player_destroyed(player_instance_global_position)
signal enemy_destroyed(enemy_instance_global_position)
var destroyed = false

func _ready():
	randomize()
	var angular_velocity = Vector3(get_random_axis_rotation(),get_random_axis_rotation(),get_random_axis_rotation())
	set_angular_velocity(angular_velocity)
	set_fixed_process(true)
	
func _fixed_process(delta):
	var position = get_translation()
	set_translation(position + Vector3(0,0,1 * delta * speed * SPEED_MULTIPLIER));

func get_random_axis_rotation():
	var degrees = rand_range(0,360)
	return deg2rad(degrees)

func remove_node(node):
		node.hide()
		node.queue_free()

func scale(scale):
	var area = get_node("Area");
	area.set_scale(scale);
	
func print_debug(message):
	if debug_mode:
		print(message)

func _on_Area_area_enter( area ):
	var node = area.get_parent()
	if node.is_in_group("player"):
		print_debug("meteor hit player")
		emit_signal(PLAYER_DESTROYED_SIGNAL_ALIAS, node.get_global_transform().origin)
		emit_signal(METEOR_DESTROYED_SIGNAL_ALIAS, self.get_global_transform().origin)
		remove_node(node)
		remove_node(self)
	if node.is_in_group("meteors") && node  != self:
		print_debug("meteor hit meteor")
		emit_signal(METEOR_DESTROYED_SIGNAL_ALIAS, self.get_global_transform().origin)
		emit_signal(METEOR_DESTROYED_SIGNAL_ALIAS, node.get_global_transform().origin)
		remove_node(node)
		remove_node(self)
	if node.is_in_group("enemies") && node != self:
		print_debug("meteor hit enemy")
		emit_signal(ENEMY_DESTROYED_SIGNAL_ALIAS, self.get_global_transform().origin)
		emit_signal(METEOR_DESTROYED_SIGNAL_ALIAS, node.get_global_transform().origin)
		remove_node(node)
		remove_node(self)
