extends Particles

export var debug_mode = false

func _ready():
	pass

func _on_Lifetime_timeout():
		print_debug("explosion removed")
		remove_node(self)

func remove_node(node):
		node.hide()
		node.queue_free()

func print_debug(message):
	if debug_mode:
		print(message)
